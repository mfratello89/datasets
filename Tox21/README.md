# File Description
## tox21\_10k\_library\_info.tsv.gz
TAB-separated file with information about compounds, including NCATS id, 
Tox21 id, Pubchem CID, Pubchem SID, SMILES, CAS number and Name

## tox21\_10k\_pubchem\_fp.txt.gz
For each Pubchem SID the corresponding Pubchem Fingerprint
