import sys
import base64

def decode_cactvs(encoded):
	decoded = base64.b64decode(encoded)
	size = int.from_bytes(decoded[0:4], "big")
	assert size == 881
	binary_string = "".join(["{:08b}".format(x) for x in decoded[4:]])[:-7]
	assert len(binary_string) == 881
	return binary_string

def extract_fingerprints():
	print_next_line = False
	parse = False
	for line in sys.stdin:
		l = line.strip()
		if print_next_line:
			if parse:
				to_print = " ".join(decode_cactvs(l))
				parse = False
			else:
				to_print = l
			print(to_print, end=" ")
			print_next_line=False

		if "PUBCHEM_COMPOUND_CID" in l:
			print_next_line = True
			parse = False
			continue
		if "PUBCHEM_CACTVS_SUBSKEYS" in l:
			print_next_line = True
			parse = True
			continue
		if "PUBCHEM_OPENEYE_CAN_SMILES" in l:
			print_next_line = True
			parse = False
			continue
		if "$" in l:
			print("")
			continue
	return

if __name__ == "__main__":
	extract_fingerprints()
